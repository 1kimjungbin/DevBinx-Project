# 💻 DevBinx's Project Repository
다양한 개인 프로젝트를 브랜치 단위로 관리하기 위해 생성한 레포지토리 입니다. <br>
진행 중인 프로젝트, 완료된 프로젝트, Archived 프로젝트를 브랜치 단위로 살펴볼 수 있습니다. <br>
## 📅 Project List <br>
### 🛠️ In-Progress
**[🎨 ArtGallery-Reimagined]**
<br><br><br>
### ✅ Complete Project

<br><br><br>
### ⚰️ Archived Project
**[👕 0925 WebPortfolio]**

<br><br><br>

[🎨 ArtGallery-Reimagined]: https://github.com/DevBinx/DevBinx-Project/tree/ArtGallery-Reimagined
[👕 0925 WebPortfolio]: https://github.com/DevBinx/WebPortfolio
